from abc import ABC, abstractmethod
import pandas as pd


class AbstractCrawlResult(ABC):
    """
    Abstract base class representing a crawl result object. This object should hold all crawled data of one website.
    Add an item row using the method add. Easy export the data using the example methods to_csv, to_sql or to_s3.
    """
    def __init__(self):
        pass

    @abstractmethod
    def add_item(self, row):
        pass

    @abstractmethod
    def to_csv(self):
        pass

    @abstractmethod
    def to_sql(self):
        pass

    @abstractmethod
    def to_s3(self):
        pass


class CrawlResult(AbstractCrawlResult):
    """
    Implementation of crawl result object.
    """

    def __init__(self, columns):
        super().__init__()
        self.data = pd.DataFrame(columns=columns)

    def add_item(self, row):
        self.data = self.data.append(row, ignore_index=True)

    def to_csv(self):
        self.data.to_csv("wolle.csv", index=False, sep=";")

    def to_sql(self):
        raise NotImplementedError()

    def to_s3(self):
        raise NotImplementedError()
