from abc import ABC, abstractmethod

from selenium.common.exceptions import TimeoutException
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import config
from result import CrawlResult


class AbstractCrawlModule(ABC):
    """
    This is the abstract base class to represent any crawler class. Each crawler is defined to have a custom crawl class
    and a general get driver class (for selenium).
    """
    def __init__(self):
        pass

    @abstractmethod
    def crawl(self):
        pass

    @staticmethod
    def get_driver():
        options = Options()
        options.add_argument('--headless')
        return Firefox(options=options)


class WolleCrawler(AbstractCrawlModule):
    """
    This class implements a web crawler for the site Wolleplatz.de.
    """
    def __init__(self):
        self.url = "https://www.wollplatz.de/"

    @staticmethod
    def get_price(driver):
        box = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "pbuy-buybx")))
        return box.find_elements_by_class_name("product-price-amount")[0].text

    @staticmethod
    def get_zusammenstellung(driver):
        data = driver.find_element_by_id("pdetailTableSpecs").text.split("\n")
        data_filtered = [e for e in data if "Zusammenstellung" in e][0]
        return " ".join(data_filtered.split(" ")[1:])

    @staticmethod
    def get_nadelstaerke(driver):
        data = driver.find_element_by_id("pdetailTableSpecs").text.split("\n")
        data_filtered = [e for e in data if "Nadelstärke" in e][0]
        return " ".join(data_filtered.split(" ")[1:])

    def navigate_to_item(self, item):
        driver = self.get_driver()
        driver.get(self.url)
        search_field = driver.find_element_by_name("txtSearch")

        # search item
        search_field.clear()
        search_field.send_keys(" ".join(item))
        search_field.send_keys(Keys.RETURN)
        return driver

    def crawl(self):
        """
        Crawling the items using selenium. The items to crawl are configured in a separate config file. See config.py .
        The crawled data is handled in the result data object. This class holds the data in pandas data frame for
        further usage.
        :return: [CrawlResult] The crawled data for all configured items of wolleplatz.de .
        """
        result_data = CrawlResult(["Name", "Preis", "Zusammenstellung", "Nadelstärke"])
        for item in config.wolle_items:
            print("Crawling", item)

            # search item
            driver = self.navigate_to_item(item)

            # extract data
            row = {"Name": " ".join(item)}
            try:
                row["Preis"] = self.get_price(driver)
                row["Zusammenstellung"] = self.get_zusammenstellung(driver)
                row["Nadelstärke"] = self.get_nadelstaerke(driver)
                result_data.add_item(row)

            except TimeoutException as e:
                print("Error crawling item: {0}. This may due to the specified item could not be found on the website"
                      " or network issues as well.".format(" ".join(item)))
                print("Skipping")
            finally:
                driver.quit()

        return result_data
