import time
from abc import ABC, abstractmethod

import config
from crawler import WolleCrawler


class AbstractHandler(ABC):
    """
    Abstract base class representing a handler for all crawlers coming in the future.
    """
    def __init__(self):
        self.crawlers = []

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def config_crawlers(self):
        pass


class CrawlHandler(AbstractHandler):
    def __init__(self):
        super().__init__()
        self.config_crawlers()

    def run(self, once=True):
        """
        This method iterates all crawler objects and call their crawl method.
        :param once: If the crawler should run only once or in endless loop with time interval.
        :return: A list of CrawlResult objects.
        """
        while True:
            data = []
            for crawler in self.crawlers:
                data.append(crawler.crawl())

            # handle crawled data
            data[0].to_csv()
            if once is True:
                break

            time.sleep(config.CRAWL_INTERVAL)

    def config_crawlers(self):
        """
        Append crawlers here.
        """
        # add all crawlers we want to have here in future
        self.crawlers.append(WolleCrawler())
