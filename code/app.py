from handler import CrawlHandler

# This is the main script from where the app can be run.

handler = CrawlHandler()
handler.run(once=True)