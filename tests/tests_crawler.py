import pytest
from crawler import WolleCrawler
import config


class TestWolleCrawler:
    """
    This is basic pytest class to test the crawled item attributes of the wolle crawler. This is by far only a small
    scope of what should be tested.
    """

    @classmethod
    def setup(self):
        crawler = WolleCrawler()
        self.result = crawler.crawl()

    def test_get_preis(self):
        assert self.result.data["Preis"][0] == "8,05"

    def test_get_zusammenstellung(self):
        assert self.result.data["Zusammenstellung"][0] == "100% Baumwolle"

    def test_get_nadel(self):
        assert self.result.data["Nadelstärke"][0] == "8 mm"



