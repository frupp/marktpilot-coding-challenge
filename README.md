# Marktpilot Coding Challenge

This is my solution to the given challenge. Thanks for the oppurtunity to learn so much about wool ;)
For development I mainly worked with PyCharm, Jupyter, Python Console and Git.

## Focus
The focus of my implementation is the expandability of the code (e.g. configured items to crawl or adding more websites) and the software architecture capsuling distinct features as well.

## Used libraries
I use selenium for crawling the websites and pandas managing and exporting crawled data. The code is written to use the Firefox browser. If you run the code make sure to correctly set up geckodriver for your Firefox browser.

## Short discription
Execute the app by running
```python code/app.py```

This will start a single crawl on the configured items for Wolleplatz.de. The items to crawl are externally configured in config.py. This could be extended to get them from a database, web API etc. After the run you will see the file wolle.csv created in the directory above. This file holds the crawled data in CSV format.

The architecture consits of three main parts:
* A Handler (CrawlHandler)
* Crawler modules (WolleCrawler)
* Data holding objects (CrawlResult)

All objects are designed to be extendable and easy to be maintained by using abstract base classes (interfaces). The handler iterates over alls configured crawler modules and saves the crawled data at the end. The crawler modules implements the code crawling one special website. It crawls item-wise, each item attribute is capsuled in a specific method. If an item can not be found in crawling process, a warning is printed.
The crawled data is managed by a pandas data frame representing a table with the different items of the site in each row. The item attributes are in the columns. This data frame is capsuled by a CrawlResult object. Due to the use of pandas and objective programming we can easy extend the objects functionality to export the data to CSV, SQL, S3 and so on. The requested crawling of attribute Lieferzeit is not yet implemented. Given the architecture and implemented objects this is straightforward and can be done in several lines of code.


## Testing
Basic unit tests can be found in test directory.
